# JGang

All Kores libraries in one Maven Repository, JGang is the secondary package registry which Kores libraries are published to.

## Add to your project

Gradle (groovy):
```groovy
repositories {
    mavenCentral()
    maven {
        name = "JGang"
        url "https://gitlab.com/api/v4/projects/30392813/packages/maven"
    }
}
```

Gradle (Kotlin):
```kotlin
repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/30392813/packages/maven") {
        name = "JGang"
    }
}
```

## Available projects:

- [JwIUtils](https://gitlab.com/Kores/JwIUtils)
- [Kores](https://gitlab.com/Kores/Kores)
- [Kores-BytecodeWriter](https://gitlab.com/Kores/Kores-BytecodeWriter)
- [Kores-SourceWriter](https://gitlab.com/Kores/Kores-SourceWriter)
- [Kores-Extra](https://gitlab.com/Kores/Kores-Extra)
- [KoresProxy](https://gitlab.com/Kores/KoresProxy)
- [Redin](https://gitlab.com/Kores/Redin)
- [EventSys](https://gitlab.com/Kores/EventSys)
- [KWCommands](https://gitlab.com/Kores/KWCommands)
- [BytecodeDisassembler](https://gitlab.com/Kores/BytecodeDisassembler)
- [GenericParser](https://gitlab.com/Kores/generic-parser/)
- [KoresGenUtil](https://gitlab.com/Kores/KoresGenUtil/)
- [AdapterHelper](https://gitlab.com/Kores/AdapterHelper/)
- [Config](https://gitlab.com/Kores/Config/)
- [Atevist](https://gitlab.com/Kores/atevist/)
- [Atevist Client](https://gitlab.com/Kores/atevist-client/)
